﻿using Contract;

namespace WebInterface.Models
{
    public class Contract1Operation : IOperation
    {
        public string Run(object input)
        {
            var request = input as Contract1Request;
            var result = "contract 1 null";

            if (request != null)
                result = string.Format("Hello {0} {1}. This is a message from contract 1 (invoker)", request.FirstName,
                    request.LastName);

            return result;
        }
    }
}