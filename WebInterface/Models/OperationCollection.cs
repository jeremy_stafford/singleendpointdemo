﻿using System;
using System.Collections.Generic;
using Contract;

namespace WebInterface.Models
{
    public class OperationCollection : Dictionary<Type, IOperation>
    {
        public static Dictionary<Type, IOperation> Operations;

        static OperationCollection()
        {
            Operations = new Dictionary<Type, IOperation>();
            Operations.Add(typeof(Contract1Request), new Contract1Operation());
            Operations.Add(typeof(Contract2Request), new Contract2Operation());
        }
    }
}