﻿using System;
using System.Collections.Generic;
using System.IO;
using Contract;
using Newtonsoft.Json;
using WebInterface.Models;

namespace WebApi.Models
{
    /// <summary>
    /// A helper class for deserializing Message Operation type requests
    /// </summary>
    public class MessageOperationRequestDeserializer
    {
        private static readonly Dictionary<string, Type> Requests = RequestCollection.Requests; 

        public static object GetRequest(DemoRequest request)
        {
            if (Requests.ContainsKey(request.OperationName))
            {
                var result = JsonConvert.DeserializeObject(request.Payload, Requests[request.OperationName]);
                if (result == null) throw new ApplicationException("Deserialization Failed.");

                return result;
            }

            throw new InvalidOperationException(string.Format("No operation was found for {0}", request.OperationName));
        }

        public static T GetRequest<T>(DemoRequest request)
            where T: class, new()
        {
            var result = JsonConvert.DeserializeObject<T>(request.Payload);
            if(result == null || result == default (T))
                throw new InvalidDataException(string.Format("Payload invalid, deserialization failed."));

            return result;
        }

        public static string DeserializeAndRun(DemoRequest request)
        {
            var contract = GetRequest(request);
            return OperationInvoker.RunOperation(contract);
        }
    }
}