﻿using System;
using System.Collections.Generic;

namespace WebInterface.Models
{
    public class OperationInvoker
    {
        private static readonly Dictionary<Type, IOperation> Operations = OperationCollection.Operations; 
        public static string RunOperation(object request)
        {
            var requestType = request.GetType();

            string result;
            if (Operations.ContainsKey(requestType))
            {
                result = Operations[requestType].Run(request);
            }
            else
            {
                throw new ApplicationException(string.Format("No operation found for {0}", requestType));
            }

            return result;
        }
    }
}