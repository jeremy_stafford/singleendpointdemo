﻿using Contract;

namespace WebInterface.Models
{
    public class Contract2Operation : IOperation
    {

        public string Run(object input)
        {
            var request = input as Contract2Request;
            var result = "contract 2 null";

            if(request != null)
                result = string.Format(request.Message + " (invoker).", request.Number1, request.Number2, request.Number1 + request.Number2);

            return result;
        }
    }
}