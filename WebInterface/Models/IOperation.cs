﻿namespace WebInterface.Models
{
    public interface IOperation
    {
        string Run(object input);    
    }
}