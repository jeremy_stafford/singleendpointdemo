﻿using System;
using System.Collections.Generic;
using Contract;

namespace WebApi.Models
{
    public class RequestCollection : Dictionary<string, Type>
    {
        public static Dictionary<string, Type> Requests;

        static RequestCollection()
        {
            Requests = new Dictionary<string, Type>
            {
                // just add new contracts here
                {"contract1-v1", typeof (Contract1Request)},
                {"contract2-v1", typeof (Contract2Request)}
            };
        }
    }
}