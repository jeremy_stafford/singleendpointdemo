﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Contract;
using WebApi.Models;

namespace WebInterface.Controllers
{
    public class HomeController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage GetMessage(DemoRequest request)
        {
            var message = "Payload Null";
            if (ModelState.IsValid)
            {
                message = MessageOperationRequestDeserializer.DeserializeAndRun(request);
            }

            var response = Request.CreateResponse(HttpStatusCode.OK, message);

            return response;
        }
    }
}
