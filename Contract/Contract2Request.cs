﻿namespace Contract
{
    public class Contract2Request
    {
        public int Number1 { get; set; }
        public int Number2 { get; set; }
        public string Message { get; set; }
    }
}