﻿namespace Contract
{
    public class DemoRequest
    {
        public string OperationName { get; set; }
        public string Payload { get; set; }
    }
}
