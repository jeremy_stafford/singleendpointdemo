﻿using System;

namespace CallingApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            // invoke greeting
            DemoOperationInvoker.ShowMessage(MessageRequestType.Greeting);

            Console.ReadKey();

            // invoke addition message
            DemoOperationInvoker.ShowMessage(MessageRequestType.Addition);

            Console.ReadKey();
        }
    }
}
