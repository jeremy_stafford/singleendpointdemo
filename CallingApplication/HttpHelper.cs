﻿using Contract;
using RestSharp;

namespace CallingApplication
{
    public class HttpHelper
    {
        public static string MakeRequest(DemoRequest request)
        {
            var client = new RestClient("http://localhost:35255");
            var restRequest = new RestRequest("Home/GetMessage", Method.POST) {RequestFormat = DataFormat.Json};
            restRequest.AddBody(request);
            var response = client.Execute(restRequest);
            return response.Content;
        }
    }
}