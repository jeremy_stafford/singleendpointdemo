﻿namespace CallingApplication
{
    public abstract class ContractInvoker
    {
        public int MessageId { get; set; }
        public abstract string GetMessage();
    }
}