﻿using Contract;

namespace CallingApplication
{
    public class Contract1Invoker : ContractInvoker
    {
        public Contract1Invoker()
        {
            MessageId = 1;
        }
        public override string GetMessage()
        {
            var contract1Request = new Contract1Request();
            contract1Request.FirstName = "Jeremy";
            contract1Request.LastName = "Stafford";
            var request = ApiRequestHelperFactory.GetRequest("contract1-v1", contract1Request);
            return HttpHelper.MakeRequest(request);
        }
    }
}