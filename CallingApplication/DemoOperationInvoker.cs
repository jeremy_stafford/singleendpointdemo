﻿using System;

namespace CallingApplication
{
    public class DemoOperationInvoker
    {
        public static void ShowMessage(MessageRequestType messageType)
        {
            var requests = new DemoRequests();
            var invoker = requests[messageType];

            var message = invoker == null 
                ? "No Invoker found" 
                : invoker.GetMessage(); // executes the message command
            
            Console.WriteLine(message);
        }
    }
}