﻿using Contract;
using Newtonsoft.Json;

namespace CallingApplication
{
    class ApiRequestHelperFactory
    {
        public static DemoRequest GetRequest(string operationName, object payload)
        {
            return new DemoRequest
            {
                OperationName = operationName,
                Payload = JsonConvert.SerializeObject(payload)
            };
        }
    }
}
