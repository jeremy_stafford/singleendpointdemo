﻿using System.Collections.Generic;

namespace CallingApplication
{
    public class DemoRequests : Dictionary<MessageRequestType, ContractInvoker>
    {
        public DemoRequests()
        {
            // add new invokers here
            Add(MessageRequestType.Addition, new Contract2Invoker());
            Add(MessageRequestType.Greeting, new Contract1Invoker());
        }
    }
}