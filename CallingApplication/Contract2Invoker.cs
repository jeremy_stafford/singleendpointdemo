﻿using Contract;

namespace CallingApplication
{
    public class Contract2Invoker : ContractInvoker
    {
        public Contract2Invoker()
        {
            MessageId = 2;
        }

        public override string GetMessage()
        {
            var request = new Contract2Request {Message = "{0} + {1} = {2}", Number1 = 12, Number2 = 21};

            var demoRequest = ApiRequestHelperFactory.GetRequest("contract2-v1", request);
            return HttpHelper.MakeRequest(demoRequest);
        }
    }
}